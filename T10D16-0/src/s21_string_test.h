#ifndef S21_STRING_TEST_H
#define S21_STRING_TEST_H
void s21_strlen_test();
void s21_strcmp_test();
void s21_strcpy_test();
void s21_strcat_test();
void s21_strchr_test();
void s21_strstr_test();
void s21_strtok_test();

void output_strlen(char *input, int output, int result);
void output_strcmp(char *input1, char *input2, int output, int result);
void output_strcpy(char *input, char *result);
#endif
